from Tkinter import *

    
class Gomoku():
    
    
    def __init__(self, master, blackstates = [], whitestates = [], move = "human"):
        self.centers = []
        for i in range(476):
            for j in range(476):
                if center(i, j) != "out of bounds":
                    self.centers += [(center(i, j)[0], center(i, j)[1])]
        self.blackstates = blackstates
        self.whitestates = whitestates
        self.move = move
        self.frame = Frame(master)
        self.frame.pack()
        self.canvas = Canvas(self.frame, width = 475, height = 475, bg = "yellow")
        self.canvas.pack()
        def callback(event):
            if [(center(event.x, event.y)[0], center(event.x, event.y)[1])] not in self.blackstates:
                self.canvas.create_oval(center(event.x, event.y)[0] - 12, center(event.x, event.y)[1] + 12, center(event.x, event.y)[0] + 12, center(event.x, event.y)[1] - 12, fill = "black")
                self.canvas.pack(side = "top", fill = "both", expand = True, padx = 2, pady = 2)
                self.whitestates += [(center(event.x, event.y)[0], center(event.x, event.y)[1])]
            else:
                self.canvas.bind("<Button-1>", callback)
                self.canvas.pack()
                    
                

        for i in range(1, 17):
            self.canvas.create_line(i*25, 25, i*25, 400)
            self.canvas.create_line(25, i*25, 400, i*25)
        

        if self.move == "human":
            self.canvas.bind("<Button-1>", callback)
            self.canvas.pack()
        
            
            
            


        def center(x1,y1):
            for i in range(0, 16):
                if i * 25 + 12 <= x1 and (i + 1) * 25 + 12 >= x1:
                    if i * 25 + 12 < x1:
                        x = (i + 1) * 25
                    else:
                        x = i * 25
                else:
                    return "out of bounds"
            for i in range(0, 16):
                if i * 25 + 12 <= y1 and (i + 1) * 25 + 12 >= y1:
                    if i * 25 + 12 < y1:
                        y = (i + 1) * 25 
                    else:
                        y = i * 25
                else:
                    return "out of bounds"
            return [x, y]


        
    
        

    
    class StateTree():
        def __init__(self, base):
            self.whites = base[0]
            self.blacks = base[1]
            self.secondlevel = []
        def second_level(base):
            sndlvl = []
            for i in self.centers:
                if i not in self.whites:
                    if i not in self.blacks:
                        for j in self.centers:
                            if j not in self.whites:
                                if j not in self.blacks:
                                    if [(self.whites, self.whites+j), (self.blacks, self.blacks + i)] not in sndlvl:
                                        sndlvl += [(self.whites, self.whites + j), (self.blacks, self.blacks + i)]
            self.secondlevel = sndlvl                                    
            
            
            

            
    


        
root = Tk()
app = Gomoku(root)
root.mainloop()
Gomoku(app)
